from flask import Flask
import os
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
migrate = Migrate()


def create_app():
    app = Flask(__name__)
    db.init_app(app)
    migrate.init_app(app, db, include_schemas=True)
    return app


# app = Flask(__name__)
database = create_app()

database.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DATABASE_URL")
# database.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DATABASE_URL").replace("postgres://",
#                                                                                     "postgresql+psycopg2://",
#                                                                                     1)
database.config['SECRET_KEY'] = os.environ.get("KEY")
database.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    password = db.Column(db.Text, nullable=False)
    number = db.Column(db.Text, nullable=True)
    email = db.Column(db.String(120), unique=True, nullable=False)
    token = db.Column(db.Text, nullable=True, default=None)
    has_paid = db.Column(db.Boolean, nullable=False, default=False)
    exp_date = db.Column(db.DateTime, nullable=True)
    profile = db.Column(db.Text, nullable=True, default=None)
    address = db.Column(db.Text, nullable=True, default=None)
    created_at = db.Column(db.DateTime, nullable=False)
    updated_at = db.Column(db.DateTime, nullable=False)
    tool = db.Column(db.Integer, nullable=False)
    tracks = db.relationship("Track", backref="user-tool", lazy='subquery', cascade="all, delete-orphan")


class Track(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_at = db.Column(db.DateTime, nullable=False)
    updated_at = db.Column(db.DateTime, nullable=False)
    turbidity = db.Column(db.Float, nullable=False)
    ph = db.Column(db.Float, nullable=False)
    van_1 = db.Column(db.Boolean, nullable=False)
    van_2 = db.Column(db.Boolean, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)



