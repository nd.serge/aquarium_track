import json

from flask import request, jsonify, make_response

from flask_restful import Resource
from models import User, UserSchema, ValidationError, LoginSchema
from flask_jwt_extended import jwt_required,get_jwt_identity

BAD_REQUEST_CODE = 400
UNAUTHORIZED_CODE = 401
NOT_FOUND = 404
SUCCESS_CODE = 200
INTERNAL_SERVER = 500


class SignUp(Resource):
    def post(self):
        data = json.loads(request.data)
        schema = UserSchema()
        try:
            result = schema.load(data)
        except ValidationError as err:
            return make_response(jsonify(err.messages), BAD_REQUEST_CODE)
        if User().find_by_email(data["email"]) is not None:
            return make_response(jsonify({"data": "email already used"}), BAD_REQUEST_CODE)
        if User().find_by_name(data["name"]) is not None:
            return make_response(jsonify({"data": "name already used"}), BAD_REQUEST_CODE)
        user = User().create_user(
            name=data["name"],
            email=data["email"],
            password=data["password"],
            number=data["number"],
            address=data["address"] if "address" in data.keys() else None,
        )
        resp = user.save_to_db()
        if resp["data"]:
            data = {
                "message": resp["message"],
                "token_life": 43200
            }
            return make_response(jsonify({"data": data}), SUCCESS_CODE)
        else:
            return make_response(jsonify({"data": resp["message"]}), INTERNAL_SERVER)


class Login(Resource):
    def post(self):
        try:
            data = json.loads(request.data)
            schema = LoginSchema()
            try:
                result = schema.load(data)
            except ValidationError as err:
                return make_response(jsonify(err.messages), BAD_REQUEST_CODE)

            message, info = User().auth(data["password"], data["email"])
            info["token_life"] = 43200
            if message == "":
                return make_response(jsonify({"data": info}), SUCCESS_CODE)
            else:
                return make_response(jsonify({"data": message}), BAD_REQUEST_CODE)
        except Exception as e:
            return make_response(jsonify({"message": e}), BAD_REQUEST_CODE)




    @jwt_required()
    def get(self):
        user = User().find_by_email(email=get_jwt_identity())
        if user is None:
            return make_response(jsonify({"data": False}), UNAUTHORIZED_CODE)
        else:
            return make_response(jsonify({"data": True}), SUCCESS_CODE)





