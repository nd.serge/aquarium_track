from flask import Flask
from flask_jwt_extended import jwt_required, get_jwt_identity, get_current_user, JWTManager
from flask_restful import Api
import os
from models import db
from login import SignUp, Login
from module import ToolResource, TracksResource


def create_app(db):
    app = Flask(__name__)
    db.init_app(app)
    api = Api(app)
    jwt = JWTManager(app)
    return app, jwt, api


# app = Flask(__name__)
app, jwt, api = create_app(db)

app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DATABASE_URL")
# app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DATABASE_URL").replace("postgres://",
# "postgresql+psycopg2://", 1)
app.config['SECRET_KEY'] = os.environ.get("KEY")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['PROPAGATE_EXCEPTIONS'] = True

api.add_resource(Login, "/api/login")
api.add_resource(SignUp, "/api/signup")
api.add_resource(ToolResource, "/api/tool")
api.add_resource(TracksResource, "/api/track")

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
