import math
import pytz
from datetime import datetime, timezone

from flask_sqlalchemy import SQLAlchemy
from typing import List
from marshmallow import Schema, fields, ValidationError
from flask import Flask
import os
# from dotenv import load_dotenv
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime, timedelta
from flask_jwt_extended import create_access_token
LOCAL_TZ = pytz.timezone('Africa/Douala')

# load_dotenv()
TOOL_ID = 1
db = SQLAlchemy()

# database = Flask(__name__)
# database.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DATABASE_URL").replace("postgres://",
#                                                                                     "postgresql+psycopg2://",
#                                                                                     1)
# database.config['SECRET_KEY'] = os.environ.get("KEY")


class UserSchema(Schema):
    name = fields.String()
    email = fields.Email(required=True, allow_none=False)
    number = fields.String(required=True, allow_none=False)
    password = fields.String(required=True, allow_none=False)
    address = fields.String()


class UserModel(Schema):
    name = fields.String()
    email = fields.Email(required=True, allow_none=False)
    address = fields.String()


class LoginSchema(Schema):
    email = fields.Email(required=True, allow_none=False)
    password = fields.String(required=True, allow_none=False)


class TrackSchema(Schema):
    id = fields.Integer()
    created_at = fields.Method("utc_to_create")
    updated_at = fields.Method("utc_to_update")
    user_id = fields.Method("get_user")
    turbidity = fields.Float()
    ph = fields.Float()
    van_1 = fields.Boolean()
    van_2 = fields.Boolean()

    def utc_to_create(self, obj):
        local_dt = obj.created_at.replace(tzinfo=timezone.utc).astimezone(tz=LOCAL_TZ)
        return local_dt.strftime("%Y-%m-%dT%H:%M:%S")

    def utc_to_update(self, obj):
        local_dt = obj.updated_at.replace(tzinfo=timezone.utc).astimezone(tz=LOCAL_TZ)
        return local_dt.strftime("%Y-%m-%dT%H:%M:%S")

    def get_user(self, obj):
        return UserModel().dump(User().find_by_id(obj.user_id))


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True, autoincrement=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    password = db.Column(db.Text, nullable=False)
    number = db.Column(db.Text, nullable=True)
    email = db.Column(db.String(120), unique=True, nullable=False)
    token = db.Column(db.Text, nullable=True, default=None)
    has_paid = db.Column(db.Boolean, nullable=False, default=False)
    exp_date = db.Column(db.DateTime, nullable=True)
    profile = db.Column(db.Text, nullable=True, default=None)
    address = db.Column(db.Text, nullable=True, default=None)
    created_at = db.Column(db.DateTime, nullable=False)
    updated_at = db.Column(db.DateTime, nullable=False)
    tool = db.Column(db.Integer, nullable=False)
    tracks = db.relationship("Track", backref="user-tool", lazy='subquery', cascade="all, delete-orphan")

    def create_user(self, name, email, password, number, address, tool=TOOL_ID):
        access_token = create_access_token(identity=email,
                                           expires_delta=timedelta(hours=12))
        self.name = name
        self.number = number
        self.email = email
        self.password = generate_password_hash(password, method="pbkdf2:sha256")
        self.created_at = datetime.now()
        self.updated_at = datetime.now()
        self.token = access_token
        self.address = address
        self.tool = tool
        return self

    def __repr__(self):
        return f"User(id={self.id},name={self.name},email={self.email},address={self.address})"

    @classmethod
    def find_by_token(cls, token) -> "User":
        return cls.query.filter_by(token=token).first()

    @classmethod
    def find_by_id(cls, id) -> "User":
        return cls.query.filter_by(id=id).first()

    @classmethod
    def find_by_email(cls, email) -> "User":
        return cls.query.filter_by(email=email).first()

    @classmethod
    def find_by_name(cls, name) -> "User":
        return cls.query.filter_by(name=name).first()

    @classmethod
    def find_all(cls) -> List["User"]:
        return cls.query.all()

    @classmethod
    def auth(cls, password, email):
        message = ""
        user = cls.find_by_email(email)
        data = {}
        if user is not None:
            if check_password_hash(user.password, password):
                access_token = create_access_token(identity=email,
                                                   expires_delta=timedelta(hours=12))
                data = {
                    "token": access_token,
                    "user": UserModel().dump(user)
                }
                user.token = access_token
                user.update_to_db()
                return message, data
            else:
                message = "invalid password"
                return message, data
        else:
            message = "invalid email"
            return message, data

    def save_to_db(self) -> dict:
        try:
            db.session.add(self)
            db.session.commit()
            return {"data": True,
                    "message": self.token}
        except Exception as e:
            print(f"error {e}")
            return {"data": False,
                    "message": e}

    def delete_to_db(self) -> bool:
        try:
            db.session.delete(self)
            db.session.commit()
            return True
        except Exception as e:
            print(f"error {e}")
            return False

    def update_to_db(self) -> bool:
        try:
            self.updated_at = datetime.now()
            db.session.commit()
            return True
        except Exception as e:
            print(f"error {e}")
            return False


class Track(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_at = db.Column(db.DateTime, nullable=False)
    updated_at = db.Column(db.DateTime, nullable=False)
    turbidity = db.Column(db.Float, nullable=False)
    ph = db.Column(db.Float, nullable=False)
    van_1 = db.Column(db.Boolean, nullable=False)
    van_2 = db.Column(db.Boolean, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)

    def create_track(self, turbidity, user_id, ph, van_1, van_2):
        self.turbidity = turbidity
        self.user_id = user_id
        self.ph = ph
        self.van_1 = van_1
        self.van_2 = van_2
        self.updated_at = datetime.now()
        self.created_at = datetime.now()
        return self

    def __repr__(self):
        return f"Track(id={self.id},ph={self.ph},turbidity={self.turbidity},van_1={self.van_1},van_2={self.van_2} " \
               f"user={self.user_id})"

    @classmethod
    def find_all(cls) -> List["Track"]:
        return cls.query.all()

    # @classmethod
    # def find_by_user(cls, id) -> List["Tracks"]:
    #     return cls.query.filter_by(user_id=id).all()

    def save_to_db(self) -> tuple:
        message = ""
        try:
            db.session.add(self)
            db.session.commit()
            return True, message
        except Exception as e:
            message = str(e)
            return False, message

    def delete_to_db(self) -> bool:
        try:
            db.session.delete(self)
            db.session.commit()
            return True
        except Exception as e:
            print(f"error {e}")
            return False

    def update_to_db(self) -> bool:
        try:
            self.updated_at = datetime.now()
            db.session.commit()
            return True
        except Exception as e:
            print(f"error {e}")
            return False


