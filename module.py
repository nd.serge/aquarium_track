from flask import request, jsonify, make_response

from flask_restful import Resource
from models import User, UserSchema, TrackSchema, Track, ValidationError
from flask_jwt_extended import jwt_required, get_jwt_identity
import json

BAD_REQUEST_CODE = 400
UNAUTHORIZED_CODE = 401
NOT_FOUND = 404
SUCCESS_CODE = 200
INTERNAL_SERVER = 500
THRESHOLD = 440


class ToolResource(Resource):
    def get(self):
        try:
            turbidity = float(request.args.get("turbidity"))
            ph = float(request.args.get("ph"))
            van_1 = bool(request.args.get("van_1"))
            van_2 = bool(request.args.get("van_2"))
            user_id = request.args.get("user")
        except Exception as e:
            print(e)
            return make_response(jsonify({"data": str(e)}))

        track = Track().create_track(

            user_id=user_id,
            turbidity=turbidity,
            ph=ph,
            van_1=van_1,
            van_2=van_2
        )

        resp = track.save_to_db()
        if resp[0]:
            return make_response(jsonify({"data": True}), SUCCESS_CODE)
        else:
            return make_response(jsonify({"data": resp[1]}), INTERNAL_SERVER)


class TracksResource(Resource):
    @jwt_required()
    def get(self):
        address = ""
        data = []
        user = User().find_by_email(get_jwt_identity())
        if user is None:
            return make_response(jsonify({"data": "invalid user"}), NOT_FOUND)

        tracks = user.tracks
        if len(tracks) == 0:
            return make_response(jsonify({"data": []}), SUCCESS_CODE)

        track = tracks[-1]

        return make_response(jsonify({"data": TrackSchema().dump(track)}), SUCCESS_CODE)

